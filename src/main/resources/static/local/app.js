var stompClient = null;

function setConnected(connected) {
    $("#connect").prop("disabled", connected);
    $("#disconnect").prop("disabled", !connected);
    if (connected) {
        $("#OutputTable").show();
    }
    else {
        $("#OutputTable").hide();
    }
    $("#Output").html("");
}

function connect() {
    var socket = new SockJS('/gs-guide-websocket');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        setConnected(true);
        console.log('Connected: ' + frame);
        stompClient.subscribe('/user/queue/sessions', function(response) {
        	showOutput(JSON.parse(response.body));
        });
    });
}

function disconnect() {
    if (stompClient != null) {
        stompClient.disconnect();
    }
    setConnected(false);
    console.log("Disconnected");
}

function sendInit() {
    stompClient.send("/app/initiations", {}, JSON.stringify({'authorization': $("#jwt").val()}));
}

function sendAnswer() {
	stompClient.send("/app/challenges", {}, JSON.stringify({'prompt': $("#challenge").val(), "answer": $("#answer").val()}));
}

function showOutput(msgObject) {
	
	var type = msgObject.type;
	var msg = msgObject.message;
	
	switch(type)
	{
	case "INITIATION":
	    $("#output").append("<tr><td>Your Username: " + msg.username + "</td></tr>");
	    $("#output").append("<tr><td>Your Session Id: " + msg.sessionId + "</td></tr>");
	    $("#output").append("<tr><td>Your Process Token: " + msg.procToken + "</td></tr>");
	    break;
	case "ACCOUNT_LIST":
		$("#output").append("<tr><td>You received an account list</td></tr>");
		break;
	case "ACCOUNT_DETAILS":
		$("#output").append("<tr><td>You received an account detail</td></tr>");
		break;
	case "STATUS":
	    $("#output").append("<tr><td>Status Update: " + msg.status + "</td></tr>");
	    $("#output").append("<tr><td>Status Descr : " + msg.description + "</td></tr>");
	    break;
	case "CHALLENGE":
		$("#output").append("<tr><td>Challenge Received.  Waiting for Answer </td></tr>");
		$("#challenge").text(msg.prompt);
		break;
	}
	
}

$(function () {
    $("form").on('submit', function (e) {
        e.preventDefault();
    });
    $( "#connect" ).click(function() { connect(); });
    $( "#disconnect" ).click(function() { disconnect(); });
    $( "#send" ).click(function() { sendInit(); });
    $( "#send-question").click(function() { sendAnswer(); });
});

