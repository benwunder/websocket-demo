var stompClient = null;

function setConnected(connected) {
    $("#connect").prop("disabled", connected);
    $("#disconnect").prop("disabled", !connected);
    if (connected) {
        $("#OutputTable").show();
    }
    else {
        $("#OutputTable").hide();
    }
    $("#Output").html("");
}

function connect() {
    var socket = new SockJS('/gateway/websocket');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        setConnected(true);
        console.log('Connected: ' + frame);
        stompClient.subscribe('/user/queue/sessions', function(response) {
        	showOutput(JSON.parse(response.body));
        });
    });
}

function disconnect() {
    if (stompClient != null) {
        stompClient.disconnect();
    }
    setConnected(false);
    console.log("Disconnected");
}

function sendInit() {
    stompClient.send("/gateway/app/initiations", {}, JSON.stringify({'authorization': $("#jwt").val(), 'swiftcode': $("#swiftcode").val(), 'username': $("#username").val(), 'password': $("#password").val()}));
}

function sendAnswer() {
	stompClient.send("/gateway/app/challenges", {}, JSON.stringify({'prompt': $("#challenge").val(), "answer": $("#answer").val()}));
}

function sendAccountSelection() {
	stompClient.send("/gateway/app/accountSelection", {}, JSON.stringify({'index': $("#account-selection").val()}))
}

function showOutput(msgObject) {
	
	var type = msgObject.type;
	var msg = msgObject.message;
	
	switch(type)
	{
	case "INITIATION":
	    $("#output").append("<tr><td>Your Socket Username: " + msg.username + "<br/>" + msg.sessionId + "<br/>" + msg.procToken + "</td></tr>");
	    break;
	case "ACCOUNT_LIST":
		$("#account-box").show();
		var accounts = msg.accounts;
		$("#account-list").html("Please select an account: <br/>");
		for(var i = 0; i < accounts.length; i++)
		{
			$("#account-list").append(accounts[i].index + ") " + accounts[i].description + " (Balance: $" + accounts[i].balance + "/ Available: $" + accounts[i].available + ")<br/>");
		}
		$("")
		break;
	case "ACCOUNT_DETAILS":
		$("#output").append("<tr><td>You received an account detail</td></tr>");
		break;
	case "STATUS":
	    $("#status-text").text(msg.status);
	    $("#status-descr").text(msg.description);
		//$("#output").append("<tr><td>Status Update: " + msg.status + "<br/>" + msg.description + "</td></tr>");
	    break;
	case "CHALLENGE":
		$("#question-box").show();
		$("#output").append("<tr><td>Challenge Received.  Waiting for Answer </td></tr>");
		$("#challenge").text(msg.prompt);
		break;
	default:
		$("#output").append("<tr><td>Unkown Message.  Body: " + JSON.stringify(msg) + "</td></tr>");
	}
	
}

$(function () {
    $("form").on('submit', function (e) {
        e.preventDefault();
    });
    $( "#connect" ).click(function() { connect(); });
    $( "#disconnect" ).click(function() { disconnect(); });
    $( "#send" ).click(function() { sendInit(); });
    $( "#send-question").click(function() { sendAnswer(); });
});

