package com.connexus.gateway.controllers;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;

import com.connexus.common.exceptions.DataIntegrityException;
import com.connexus.common.model.request.SessionAccountRequest;
import com.connexus.common.model.request.SessionAccountsListRequest;
import com.connexus.common.model.request.SessionChallengeRequest;
import com.connexus.common.model.request.SessionRequest;
import com.connexus.common.model.request.SessionStatusRequest;
import com.connexus.common.model.response.SessionChallengeResponse;
import com.connexus.common.model.response.SessionResponse;
import com.connexus.common.model.response.SessionStatusResponse;
import com.connexus.common.utils.SessionUtil.Status;
import com.connexus.gateway.dbmodels.BankSession;
import com.connexus.gateway.exceptions.BankSessionNotFoundException;
import com.connexus.gateway.models.AccountSelection;
import com.connexus.gateway.models.MessageWrapper;
import com.connexus.gateway.models.MessageWrapper.MessageType;
import com.connexus.gateway.repository.services.BankSessionService;
import com.connexus.gateway.temp.SessionInitationRequest;

@Controller
public class BankSessionController {

	private final String SUBSCRIPTION_PATH = "/queue/sessions";
	private final String SEND_TO_USER_PATH = "/queue/sessions";
	// TODO: Configuration for ORCHESTRATOR_URL
	private final String ORCHESTRATOR_URL = "http://10.1.1.133:8080/orchestrator-1.0/";
	// TODO: Get self url from configuration
	private final String SELF_URL = "http://10.1.101.72:8080/gateway/";

	@Autowired
	BankSessionService bankSessionService;

	/**
	 * This is the template used to send messages to the user
	 */
	private SimpMessagingTemplate template;

	/**
	 * This constructor autowires the SimpMessagingTemplate to tell Spring to
	 * inject it for us. The bean is created in the websocket namespace
	 * 
	 * @param template
	 */
	@Autowired
	public BankSessionController(SimpMessagingTemplate template) {
		this.template = template;
	}

	/*
	 * *****************************************************
	 * 
	 * Methods for Communicating with the User
	 * 
	 *****************************************************/

	@MessageMapping("/challenges")
	@SendToUser(destinations = SUBSCRIPTION_PATH, broadcast = false)
	public MessageWrapper<Boolean> answerChallenge(SessionChallengeRequest challenge, Principal principal) throws DataIntegrityException, BankSessionNotFoundException {
		BankSession session = bankSessionService.getEntityByUser(principal);

		if (session == null)
			throw new BankSessionNotFoundException("No Session Found matching that process token");

		session.setChallengeResponse(challenge.getAnswer());

		bankSessionService.update(session);

		return new MessageWrapper<Boolean>(MessageType.CHALLENGE_ACCEPTED, true);
	}

	@MessageMapping("/initiations")
	@SendToUser(destinations = SUBSCRIPTION_PATH, broadcast = false)
	public MessageWrapper<SessionRequest> beginProcess(SessionInitationRequest message, Principal principal) throws DataIntegrityException {

		String sessionsInitiateURL = ORCHESTRATOR_URL + "/sessions";
		System.out.println("Authorization: " + message.getAuthorization());
		System.out.println("USername: " + message.getUsername());
		System.out.println("Password: " + message.getPassword());
		// TODO: Discover user's session id based on JWT

		RestTemplate restTemplate = new RestTemplate();
		SessionRequest sessionRequest = new SessionRequest();
		sessionRequest.setGatewayCallbackURL(SELF_URL);
		SessionResponse sessionResponse = restTemplate.postForObject(sessionsInitiateURL, sessionRequest, SessionResponse.class);

		String procToken = sessionResponse.getProcToken();
		String sessionsStartURL = ORCHESTRATOR_URL + String.format("/sessions/%s", procToken);

		BankSession session = bankSessionService.getEntityByProcToken(procToken);

		if (session == null) {
			session = new BankSession();
			session.setSessionId("SessionId");
			session.setProcToken(procToken);
			session.setSocketUsername(principal.getName());
			session.setStatus(Status.READY);
			bankSessionService.create(session);
		} else {
			throw new DataIntegrityException("Specified BankSession already exists");
		}

		SessionRequest startRequest = bankSessionService.convertToSessionRequest(session);

		startRequest.setGatewayCallbackURL(SELF_URL);
		startRequest.setSwiftcode(message.getSwiftcode());
		startRequest.setUsername(message.getUsername());
		startRequest.setPassword(message.getPassword());

		restTemplate.put(sessionsStartURL, startRequest);

		return new MessageWrapper<SessionRequest>(MessageType.INITIATION, startRequest);

	}

	@MessageMapping("/accountSelection")
	@SendToUser(destinations = SUBSCRIPTION_PATH, broadcast = false)
	public MessageWrapper<BankSession> selectAccount(AccountSelection message, Principal principal) throws DataIntegrityException {

		// TODO: Do a Request to Orchestrator regarding the account

		return null;
	}
	/*
	 * ******************************************************
	 * 
	 * Methods to accept incoming updates from Orchestrator
	 * 
	 ******************************************************/

	@RequestMapping(value = "/sessions/{procToken}/accounts", method = RequestMethod.POST)
	public ResponseEntity<?> receiveAccountsDescription(@PathVariable(name = "procToken") String procToken, @RequestBody SessionAccountsListRequest rq)
			throws BankSessionNotFoundException, DataIntegrityException {

		BankSession session = bankSessionService.getEntityByProcToken(procToken);

		if (session == null)
			throw new BankSessionNotFoundException("No Session Found matching that process token");

		this.template.convertAndSendToUser(session.getSocketUsername(), SEND_TO_USER_PATH, new MessageWrapper<SessionAccountsListRequest>(MessageType.ACCOUNT_LIST, rq));
		return null;
	}

	@RequestMapping(value = "/sessions/{procToken}/accounts/{index}", method = RequestMethod.PUT)
	public ResponseEntity<?> receiveAccountDetail(@PathVariable(name = "index") int index, @PathVariable(name = "procToken") String procToken, @RequestBody SessionAccountRequest rq)
			throws BankSessionNotFoundException, DataIntegrityException {

		BankSession session = bankSessionService.getEntityByProcToken(procToken);

		if (session == null)
			throw new BankSessionNotFoundException("No Session Found matching that process token");

		this.template.convertAndSendToUser(session.getSocketUsername(), SEND_TO_USER_PATH, new MessageWrapper<SessionAccountRequest>(MessageType.ACCOUNT_DETAILS, rq));
		return null;
	}

	@RequestMapping(value = "/sessions/{procToken}/status", method = RequestMethod.POST)
	public ResponseEntity<SessionStatusResponse> receiveStatusUpdate(@PathVariable(name = "procToken") String procToken, @RequestBody SessionStatusRequest rq)
			throws DataIntegrityException, BankSessionNotFoundException {

		BankSession session = bankSessionService.getEntityByProcToken(procToken);

		if (session == null)
			throw new BankSessionNotFoundException("No Session Found matching that process token");

		System.out.println("Username: " + session.getSocketUsername());
		session.setStatus(rq.getStatus());
		bankSessionService.update(session);

		this.template.convertAndSendToUser(session.getSocketUsername(), SEND_TO_USER_PATH, new MessageWrapper<SessionStatusRequest>(MessageType.STATUS, rq));

		SessionStatusResponse response = new SessionStatusResponse();
		response.setStatus(rq.getStatus().toString());
		response.setDescription(rq.getDescription());
		response.setProcToken(procToken);

		response.createLinks();
		return new ResponseEntity<SessionStatusResponse>(response, HttpStatus.OK);
	}

	@RequestMapping(value = "/sessions/{procToken}/challenge", method = RequestMethod.POST)
	public ResponseEntity<SessionChallengeResponse> receiveChallengeRequest(@PathVariable(name = "procToken") String procToken, @RequestBody SessionChallengeRequest rq)
			throws DataIntegrityException, BankSessionNotFoundException {

		BankSession session = bankSessionService.getEntityByProcToken(procToken);

		if (session == null)
			throw new BankSessionNotFoundException("No Session Found matching that process token");

		session.setChallengeResponse("");
		bankSessionService.update(session);

		this.template.convertAndSendToUser(session.getSocketUsername(), SEND_TO_USER_PATH, new MessageWrapper<SessionChallengeRequest>(MessageType.CHALLENGE, rq));

		for (int i = 0; i < 120; i++) {
			// TODO: Configure number of seconds to check
			String challengeAnswer = bankSessionService.getChallengeResponse(procToken);

			if (challengeAnswer != null && !("").equals(challengeAnswer)) {
				System.out.println("Success! Answer received");
				SessionChallengeResponse response = new SessionChallengeResponse();
				response.setPrompt(rq.getPrompt());
				response.setProcToken(procToken);
				response.setAnswer(challengeAnswer);
				response.createLinks();
				return new ResponseEntity<SessionChallengeResponse>(response, HttpStatus.OK);
			} else {
				try {
					System.out.println("Still waiting... trying " + (120 - i) + " more times");
					Thread.sleep(500);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		this.template.convertAndSendToUser(session.getSocketUsername(), SEND_TO_USER_PATH, new MessageWrapper<Boolean>(MessageType.CHALLENGE_ACCEPTED, false));
		System.out.println("No more tries");
		return null;
	}

}
