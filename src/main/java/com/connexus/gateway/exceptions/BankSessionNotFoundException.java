package com.connexus.gateway.exceptions;

public class BankSessionNotFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -966087352218363132L;

	public BankSessionNotFoundException(String msg) {
		super(msg);
	}
}
