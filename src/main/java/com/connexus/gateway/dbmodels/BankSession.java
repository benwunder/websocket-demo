package com.connexus.gateway.dbmodels;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.connexus.common.utils.SessionUtil.Status;

@Entity
@Table(name = "ws_bankSession")
public class BankSession {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int bankSessionId;
	private String socketUsername;
	private String sessionId;
	private String procToken;
	@Enumerated(EnumType.STRING)
	private Status status;
	private String challengeResponse;

	/**
	 * @return the username
	 */
	public String getSocketUsername() {
		return socketUsername;
	}

	/**
	 * @param username
	 *            the username to set
	 */
	public void setSocketUsername(String socketUsername) {
		this.socketUsername = socketUsername;
	}

	/**
	 * @return the sessionId
	 */
	public String getSessionId() {
		return sessionId;
	}

	/**
	 * @param sessionId
	 *            the sessionId to set
	 */
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	/**
	 * @return the procToken
	 */
	public String getProcToken() {
		return procToken;
	}

	/**
	 * @param procToken
	 *            the procToken to set
	 */
	public void setProcToken(String procToken) {
		this.procToken = procToken;
	}

	/**
	 * @return the status
	 */
	public Status getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(Status status) {
		this.status = status;
	}

	/**
	 * @return the challengeResponse
	 */
	public String getChallengeResponse() {
		return challengeResponse;
	}

	/**
	 * @param challengeResponse
	 *            the challengeResponse to set
	 */
	public void setChallengeResponse(String challengeResponse) {
		this.challengeResponse = challengeResponse;
	}
}
