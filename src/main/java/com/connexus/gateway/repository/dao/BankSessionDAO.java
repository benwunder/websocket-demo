package com.connexus.gateway.repository.dao;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Component;

import com.connexus.common.AbstractDAO;
import com.connexus.common.exceptions.DataIntegrityException;
import com.connexus.gateway.dbmodels.BankSession;

@Component
public class BankSessionDAO extends AbstractDAO {

	@SuppressWarnings("unchecked")
	@Transactional
	public ArrayList<BankSession> getList(BankSession filterObj) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(BankSession.class);

		if (filterObj.getProcToken() != null)
			criteria.add(Restrictions.eq("procToken", filterObj.getProcToken()));

		if (filterObj.getSessionId() != null)
			criteria.add(Restrictions.eq("sessionId", filterObj.getSessionId()));

		if (filterObj.getStatus() != null)
			criteria.add(Restrictions.eq("status", filterObj.getStatus().toString()));

		if (filterObj.getSocketUsername() != null)
			criteria.add(Restrictions.eq("socketUsername", filterObj.getSocketUsername()));

		return (ArrayList<BankSession>) criteria.list();

	}

	@Transactional
	public BankSession getEntity(String procToken) throws DataIntegrityException {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(BankSession.class);

		criteria.add(Restrictions.eq("procToken", procToken));

		@SuppressWarnings("unchecked")
		List<BankSession> results = criteria.list();

		switch (results.size()) {
		case 0:
			return null;
		case 1:
			return results.get(0);
		default:
			throw new DataIntegrityException("Too many bank sessions per process token");
		}
	}
}
