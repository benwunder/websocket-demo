package com.connexus.gateway.repository.services;

import java.security.Principal;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.connexus.common.exceptions.DataIntegrityException;
import com.connexus.common.model.request.SessionRequest;
import com.connexus.gateway.dbmodels.BankSession;
import com.connexus.gateway.repository.dao.BankSessionDAO;

@Component
public class BankSessionService {

	@Autowired
	BankSessionDAO bankSessionDAO;

	public BankSession getEntityByProcToken(String procToken) throws DataIntegrityException {
		return bankSessionDAO.getEntity(procToken);
	}

	public BankSession getEntityByUser(Principal principal) throws DataIntegrityException {
		BankSession filterObj = new BankSession();

		filterObj.setSocketUsername(principal.getName());

		ArrayList<BankSession> results = bankSessionDAO.getList(filterObj);

		switch (results.size()) {
		case 0:
			return null;
		case 1:
			return results.get(0);
		default:
			throw new DataIntegrityException(String.format("Too many sessions [%d] for given user %s", results.size(), principal.getName()));
		}
	}

	public BankSession create(BankSession object) {
		return (BankSession) bankSessionDAO.create(object);
	}

	public BankSession update(BankSession object) {
		return (BankSession) bankSessionDAO.update(object);
	}

	/**
	 * Retrieve challenge response from the database directly
	 * 
	 * @param procToken
	 *            The process Token to look up
	 * @return A challenge response if it exists
	 * @throws DataIntegrityException
	 *             If BankSession table is mucked up.
	 */
	public String getChallengeResponse(String procToken) throws DataIntegrityException {
		BankSession session = getEntityByProcToken(procToken);

		if (session == null)
			return null;
		else
			return session.getChallengeResponse();
	}

	public SessionRequest convertToSessionRequest(BankSession session) {
		SessionRequest request = new SessionRequest();
		if (session.getProcToken() != null)
			request.setProcToken(session.getProcToken());

		if (session.getStatus() != null)
			request.setStatus(session.getStatus());

		return request;
	}
}
