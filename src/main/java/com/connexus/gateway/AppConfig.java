package com.connexus.gateway;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.Executor;

import javax.sql.DataSource;

import org.hibernate.SessionFactory;
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.aop.interceptor.SimpleAsyncUncaughtExceptionHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.connexus.common.utils.SecurityUtil;

//import com.connexus.common.utils.securityUtil;

@Configuration
@EnableTransactionManagement
@EnableAsync
public class AppConfig implements AsyncConfigurer {
	@Value("${server.dbproperties.location}")
	private String dbPropertiesLocation;

	@Bean
	public LocalSessionFactoryBean sessionFactory() {
		LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
		sessionFactory.setDataSource(getDataSource());
		sessionFactory.setPackagesToScan(new String[] { "com.connexus" });
		sessionFactory.setHibernateProperties(hibernateProperties());
		return sessionFactory;
	}

	@Bean
	public DataSource getDataSource() {
		Properties prop = new Properties();
		InputStream input = null;
		String url = "", username = "", password = "", driver = "", encrypted = "";
		String filePath = this.dbPropertiesLocation;
		try {
			System.out.println("Filepath: " + filePath);
			filePath = filePath + "database.properties";
			input = new FileInputStream(filePath);
			prop.load(input);
			encrypted = prop.getProperty("encrypted");
			url = prop.getProperty("dburl");
			url += "?useSSL=false";
			driver = prop.getProperty("driver");
			if (encrypted == null || encrypted.equals("false")) {
				// get the plaintext user/pass
				username = prop.getProperty("username");
				password = prop.getProperty("password");
				// set the user/pass to the encrypted versions and tell future
				// gets that it's already been encrypted
				prop.setProperty("username", SecurityUtil.encryptString(username));
				prop.setProperty("password", SecurityUtil.encryptString(password));
				prop.setProperty("encrypted", "true");
				prop.store(new FileOutputStream(filePath), null);
			} else {
				username = SecurityUtil.decryptString(prop.getProperty("username"));
				password = SecurityUtil.decryptString(prop.getProperty("password"));
			}
			System.out.println("Driver: " + driver);
			System.out.println("URL: " + url);
			System.out.println("Username: " + username);
		} catch (IOException ex) {
			System.out.println("And IO Exception has occured when trying to read the database.properties file.");
			ex.printStackTrace();
		}
		if (url.isEmpty() || username.isEmpty() || password.isEmpty() || driver.isEmpty()) {
			System.out.println("Defaulting to default values");
			driver = "com.mysql.cj.jdbc.Driver";
			url = "jdbc:mysql://dev-test.ch69l03iiiki.us-east-1.rds.amazonaws.com:3306/dev_finserv_gateway";
			username = "dev_finserv";
			password = "Ba?u@Avaven2";
		}
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName(driver);
		dataSource.setUrl(url);
		dataSource.setUsername(username);
		dataSource.setPassword(password);
		return dataSource;
	}

	private Properties hibernateProperties() {
		Properties properties = new Properties();
		properties.put("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
		properties.put("hibernate.show_sql", false);
		properties.put("hibernate.format_sql", true);
		properties.put("hibernate.hbm2ddl.auto", "update");
		properties.put("hibernate.globally_quoted_identifiers", "true");
		return properties;
	}

	@Bean
	@Autowired
	public HibernateTransactionManager transactionManager(SessionFactory s) {
		HibernateTransactionManager txManager = new HibernateTransactionManager();
		txManager.setSessionFactory(s);
		return txManager;
	}

	@Override
	@Bean
	public Executor getAsyncExecutor() {
		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setCorePoolSize(7);
		executor.setMaxPoolSize(42);
		executor.setQueueCapacity(11);
		executor.setThreadNamePrefix("MyExecutor-");
		executor.initialize();
		return executor;
	}

	@Override
	@Bean
	public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() {
		return new SimpleAsyncUncaughtExceptionHandler();
	}
}