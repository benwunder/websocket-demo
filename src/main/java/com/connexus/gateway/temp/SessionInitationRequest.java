package com.connexus.gateway.temp;

public class SessionInitationRequest {

	private String authorization;
	private String swiftcode;
	private String username;
	private String password;

	/**
	 * @return the swiftcode
	 */
	public String getSwiftcode() {
		return swiftcode;
	}

	/**
	 * @param swiftcode
	 *            the swiftcode to set
	 */
	public void setSwiftcode(String swiftcode) {
		this.swiftcode = swiftcode;
	}

	public SessionInitationRequest() {
	}

	public SessionInitationRequest(String authorization) {
		this.authorization = authorization;
	}

	public String getAuthorization() {
		return authorization;
	}

	public void setAuthorization(String authorization) {
		this.authorization = authorization;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username
	 *            the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
}
