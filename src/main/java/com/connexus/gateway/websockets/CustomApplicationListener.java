package com.connexus.gateway.websockets;

import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.SessionConnectEvent;
import org.springframework.web.socket.messaging.SessionConnectedEvent;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;

@Component
public class CustomApplicationListener {

	@EventListener
	public void handleSessionConnectEvent(SessionConnectEvent event) {
		System.out.println("Session Connecting: " + event.toString());
	}

	@EventListener
	public void handleSessionConnected(SessionConnectedEvent event) {
		System.out.println("Session Connected: " + event.toString());
	}

	@EventListener
	public void handleSessionDisconnectEvent(SessionDisconnectEvent event) {
		System.out.println("Session Disconnecting: " + event.toString());
	}

}
