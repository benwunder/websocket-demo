package com.connexus.gateway.models;

public class MessageWrapper<T> {

	public static enum MessageType {
		INITIATION, ACCOUNT_LIST, ACCOUNT_DETAILS, CHALLENGE, STATUS, CHALLENGE_ACCEPTED
	}

	private MessageType type;
	private T message;

	public MessageWrapper(MessageType type, T message) {
		this.type = type;
		this.message = message;
	}

	/**
	 * @return the message
	 */
	public T getMessage() {
		return message;
	}

	/**
	 * @param message
	 *            the message to set
	 */
	public void setMessage(T message) {
		this.message = message;
	}

	/**
	 * @return the type
	 */
	public MessageType getType() {
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(MessageType type) {
		this.type = type;
	}
}
