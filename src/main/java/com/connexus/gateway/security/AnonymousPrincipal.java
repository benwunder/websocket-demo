package com.connexus.gateway.security;

import java.security.Principal;
import java.util.Objects;
import java.util.UUID;

public class AnonymousPrincipal implements Principal {

	private String name;

	public AnonymousPrincipal() {
		name = UUID.randomUUID().toString();
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return name;
	}

	@Override
	public boolean equals(Object another) {
		if (!(another instanceof Principal))
			return false;

		Principal principal = (Principal) another;

		return principal.getName() == this.name;

	}

	@Override
	public int hashCode() {
		return Objects.hash(name);
	}
}
